package com.parkingbackuser;

import com.parkingbackuser.adapter.UserDbAdapter;
import com.parkingbackuser.helper.UserDbHelper;
import com.parkingbackuser.mapper.UserDbMapper;
import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserMO;
import com.parkingbackuser.ports.infrastructure.UserDbPort;
import com.parkingbackuser.repository.UserRepository;
import com.parkingbackuser.service.UserService;
import com.parkingbackuser.specification.UserSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;





@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserDbTest {
    @InjectMocks
    UserDbAdapter userDbAdapter;

    @Mock
    UserRepository userRepository;

    private final UserDbMapper userDbMapper = UserDbMapper.INSTANCE;

    private UserDbHelper userDbHelper;

    @BeforeEach
    void initialize(){
        this.userDbHelper = new UserDbHelper();
    }

    @Test
    @Disabled
    void getUserListTest() {
        UserMO fake_user = userDbHelper.getUserMO();

        when(userDbAdapter.getUsers(FilterUser.builder().dni("20931845M").mail("pacofdez55@gmail.com").phone(685911592).name("Paco").lastName("Fernandez").build())).thenReturn(List.of(userDbMapper.toDomain(fake_user)));

        List<User> users = userDbAdapter.getUsers(FilterUser.builder().dni("20931845M").mail("pacofdez55@gmail.com").phone(695811592).name("Paco").lastName("Fernandez").build());

        assertEquals(users.get(0).getDni(), fake_user.getPk().getDni());
        assertEquals(users.get(0).getMail(),fake_user.getPk().getMail());
        assertEquals(users.get(0).getPhone(),fake_user.getPhone());
        assertEquals(users.get(0).getName(),fake_user.getName());
        assertEquals(users.get(0).getLastName(),fake_user.getLastName());

    }

    @Test
    void save() {
        User fake_user = userDbMapper.toDomain(userDbHelper.getUserMO());

        userDbAdapter.createUser(fake_user);

        verify(userRepository, times(1)).save(any());
    }

    @Test
    void delete(){
        User fake_user = userDbMapper.toDomain(userDbHelper.getUserMO());

        userDbAdapter.deleteUser(fake_user.getDni(),fake_user.getMail());
        verify(userRepository,times(1)).deleteById(any());
    }
}
