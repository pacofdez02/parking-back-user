package com.parkingbackuser.helper;

import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserMO;
import com.parkingbackuser.model.pk.UserPK;

public class UserDbHelper {
    private UserPK fake_pk;
    private String fake_password;
    private Integer fake_phone;
    private String fake_name;
    private String fake_lastName;

    void initializeVariables(){
        fake_pk = UserPK.builder().dni("20931845M").mail("pacofdez55@gmail.com").build();
        fake_password = "123456";
        fake_phone = 695811592;
        fake_name = "Paco";
        fake_lastName = "Fernandez";
    }


    public UserMO getUserMO(){
        initializeVariables();
        return UserMO.builder().pk(fake_pk).password(fake_password).phone(fake_phone).name(fake_name).lastName(fake_lastName).build();
    }
}
