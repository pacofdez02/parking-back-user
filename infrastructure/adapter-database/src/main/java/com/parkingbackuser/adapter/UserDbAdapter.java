package com.parkingbackuser.adapter;

import com.parkingbackuser.mapper.UserDbMapper;
import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserMO;
import com.parkingbackuser.model.pk.UserPK;
import com.parkingbackuser.ports.infrastructure.UserDbPort;
import com.parkingbackuser.repository.UserRepository;
import com.parkingbackuser.specification.UserSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserDbAdapter implements UserDbPort {

    private final UserRepository userRepository;
    private final UserDbMapper userDbMapper = UserDbMapper.INSTANCE;

    @Override
    public User getUser(String dni, String mail) {
        return userDbMapper.toDomain(userRepository.findById(userDbMapper.toMO(dni, mail)).orElseThrow());

    }

    @Override
    public List<User> getUsers(FilterUser filterUser) {
        Specification<UserMO> spec = new UserSpecification(filterUser.getDni(), filterUser.getMail(), filterUser.getPhone(), filterUser.getName(), filterUser.getLastName());
        return userRepository.findAll(spec).stream().map(userDbMapper::toDomain).collect(Collectors.toList());    }

    @Override
    public void createUser(User user) {
        userRepository.save(userDbMapper.toMO(user));
    }

    @Override
    public Boolean existsUserByPk(String dni, String mail){
        return userRepository.existsByPkDniAndPkMail(dni,mail);
    }

    @Override
    public User findUserByPk(String dni, String mail){
        return userDbMapper.toDomain(userRepository.findByPkDniAndPkMail(dni,mail));
    }



    @Override
    public void deleteUser(String dni, String mail) {
            UserPK userPk = instanceUserPK(dni,mail);
            userRepository.deleteById(userPk);
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(userDbMapper.toMO(user));
    }


    private UserPK instanceUserPK(String dni, String mail){
        return UserPK.builder()
                .dni(dni)
                .mail(mail)
                .build();
    }

    @Override
    public Boolean existsUserByPkMailAndPassword(String mail, String password) {
        return userRepository.existsByPkMailAndPassword(mail, password);
    }

    @Override
    public User findUserByPkMailAndPassword(String mail, String password) {
        return userDbMapper.toDomain(userRepository.findByPkMailAndPassword(mail, password));

    }
}
