package com.parkingbackuser.specification;

import com.parkingbackuser.model.UserMO;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class UserSpecification implements Specification<UserMO> {
    private final List<Predicate> predicates = new ArrayList<>();
    private String dni;
    private String mail;
    private Integer phone;
    private String name;
    private String lastName;

    public UserSpecification(String dni, String mail, Integer phone, String name, String lastName) {
        this.dni = dni;
        this.mail = mail;
        this.phone = phone;
        this.name = name;
        this.lastName = lastName;
    }

    @Override
    public Predicate toPredicate(Root<UserMO> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        searchByDni(root, cb);
        searchByMail(root, cb);
        searchByPhone(root, cb);
        searchByName(root, cb);
        searchByLastName(root, cb);
        return cb.and(getArrayPredicates());
    }


    private void searchByDni(Root<UserMO> root, CriteriaBuilder cb) {
        if(this.dni != null){
            predicates.add(cb.equal(root.get("pk").get("dni"), this.dni));
        }
    }

    private void searchByMail(Root<UserMO> root, CriteriaBuilder cb) {
        if(this.mail != null){
            predicates.add(cb.equal(root.get("pk").get("mail"), this.mail));
        }
    }

    private void searchByPhone(Root<UserMO> root, CriteriaBuilder cb) {
        if(this.phone != null){
            predicates.add(cb.equal(root.get("phone"), this.phone));
        }
    }

    private void searchByName(Root<UserMO> root, CriteriaBuilder cb) {
        if(this.name != null){
            predicates.add(cb.equal(root.get("name"), this.name));
        }
    }

    private void searchByLastName(Root<UserMO> root, CriteriaBuilder cb) {
        if(this.lastName != null){
            predicates.add(cb.equal(root.get("lastName"), this.lastName));
        }
    }
    Predicate[] getArrayPredicates() {
        return predicates.toArray(new Predicate[0]);
    }


}
