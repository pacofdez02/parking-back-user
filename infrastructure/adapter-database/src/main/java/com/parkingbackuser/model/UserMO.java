package com.parkingbackuser.model;

import com.parkingbackuser.model.pk.UserPK;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PARKINGUSER")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserMO {
    @EmbeddedId
    private UserPK pk;

    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "PHONE")
    private Integer phone;
    @Column(name = "NAME")
    private String name;
    @Column(name = "LASTNAME")
    private String lastName;


}
