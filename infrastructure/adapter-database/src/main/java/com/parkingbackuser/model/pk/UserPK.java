package com.parkingbackuser.model.pk;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data
public class UserPK implements Serializable {

    @Column(name = "DNI")
    private String dni;

    @Column(name = "MAIL")
    private String mail;
}
