package com.parkingbackuser.mapper;

import com.parkingbackuser.model.UserMO;
import com.parkingbackuser.model.User;
import com.parkingbackuser.model.pk.UserPK;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserDbMapper {
    UserDbMapper INSTANCE = Mappers.getMapper(UserDbMapper.class);
    @Mapping(source = "pk", target = ".")
    User toDomain (UserMO userMO);
    @Mapping(source = ".", target = "pk")
    UserMO toMO (User user);


    default UserPK toMO(String dni, String mail){
        return UserPK.builder()
                .dni(dni)
                .mail(mail)
                .build();
    }
}