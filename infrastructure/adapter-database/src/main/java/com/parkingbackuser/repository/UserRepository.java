package com.parkingbackuser.repository;

import com.parkingbackuser.model.UserMO;
import com.parkingbackuser.model.pk.UserPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository <UserMO, UserPK>, JpaSpecificationExecutor<UserMO> {
    UserMO findByPkDniAndPkMail(String dni, String mail);

    Boolean existsByPkDniAndPkMail(String dni, String mail);

    Boolean existsByPkMailAndPassword(String mail, String password);

    UserMO findByPkMailAndPassword(String mail, String password);
}
