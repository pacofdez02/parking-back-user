package com.parkingbackuser;

import com.parkingbackuser.helper.UserHelper;
import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;
import com.parkingbackuser.ports.infrastructure.UserDbPort;
import com.parkingbackuser.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;





@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserTest {
    @InjectMocks
    UserService userService;
    @Mock
    UserDbPort userDbPort;
    @Mock
    private UserHelper userHelper;
    @BeforeEach
    void initialize(){
        this.userHelper = new UserHelper();
    }
    @Test
    void getUserListTest() {
        User fake_user = userHelper.getUser();

        when(userDbPort.getUsers(any())).thenReturn(List.of(fake_user));

        List<User> users = userService.getUsers(FilterUser.builder().dni("20931845M")
                .mail("pacofdez55@gmail.com").phone(695811592)
                .name("Paco").lastName("Fernandez").build());

        assertEquals(users.get(0).getDni(), fake_user.getDni());
        assertEquals(users.get(0).getMail(),fake_user.getMail());
        assertEquals(users.get(0).getPhone(),fake_user.getPhone());
        assertEquals(users.get(0).getName(),fake_user.getName());
        assertEquals(users.get(0).getLastName(),fake_user.getLastName());
    }
}
