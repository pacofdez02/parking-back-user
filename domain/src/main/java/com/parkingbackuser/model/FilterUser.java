package com.parkingbackuser.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FilterUser {
    private String dni;
    private String mail;
    private Integer phone;
    private String name;
    private String lastName;
}
