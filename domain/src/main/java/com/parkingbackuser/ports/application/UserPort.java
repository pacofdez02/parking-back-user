package com.parkingbackuser.ports.application;

import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;

import java.util.List;

public interface UserPort {

    User getUser(String dni, String mail);
    List<User> getUsers(FilterUser filterUser);
    void createUser(User user);
    void deleteUser(String dni, String mail);

    void updateUser(String dni, String mail, User user);
    User loginUser(String mail, String password);

    }
