package com.parkingbackuser.ports.infrastructure;

import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;

import java.util.List;

public interface UserDbPort {

    User getUser(String dni, String mail);

    List<User> getUsers(FilterUser filterUser);
    void createUser(User user);
    Boolean existsUserByPk(String dni, String mail);
    User findUserByPk(String dni, String mail);
    void deleteUser(String dni, String mail);

    void updateUser(User user);
    Boolean existsUserByPkMailAndPassword(String mail, String password);

    User findUserByPkMailAndPassword(String mail, String password);




}
