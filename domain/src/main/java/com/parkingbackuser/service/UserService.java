package com.parkingbackuser.service;

import com.parkingbackuser.exception.UserException;
import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.User;
import com.parkingbackuser.ports.application.UserPort;
import com.parkingbackuser.ports.infrastructure.UserDbPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserPort {

    private final UserDbPort userDbPort;

    @Override
    @Transactional(readOnly = true)
    public User getUser(String dni, String mail) {
        return userDbPort.getUser(dni,mail);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> getUsers(FilterUser filterUser) {
        return userDbPort.getUsers(filterUser);
    }

    @Override
    @Transactional
    public void createUser(User user) {
        if(!userDbPort.existsUserByPk(user.getDni(), user.getMail()))
            userDbPort.createUser(user);
        else throw new UserException("User already exists");
    }

    @Override
    @Transactional
    public void deleteUser(String dni, String mail) {
        if(userDbPort.existsUserByPk(dni,mail)){
            userDbPort.deleteUser(dni, mail);
        }else throw new UserException("User doesn't exist");
    }

    @Override
    @Transactional
    public void updateUser(String dni,String mail, User user) {
        if(userDbPort.existsUserByPk(dni, mail)){
            if(!dni.equals(user.getDni()) || !mail.equals(user.getMail())){
                userDbPort.deleteUser(dni, mail);
            }
            user.setDni(dni);
            user.setMail(mail);
            userDbPort.updateUser(user);
        }else throw new UserException("User doesn't exist");

    }

    @Override
    @Transactional(readOnly = true)
    public User loginUser(String mail, String password){
        if(userDbPort.existsUserByPkMailAndPassword(mail, password)){
            return userDbPort.findUserByPkMailAndPassword(mail, password);
        }else throw new UserException("User doesn't exist");
    }


}
