package com.parkingbackuser.exception;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class UserException extends RuntimeException {

    private String message;

    public UserException(String message) {
        super();
        this.message = message;
    }

}
