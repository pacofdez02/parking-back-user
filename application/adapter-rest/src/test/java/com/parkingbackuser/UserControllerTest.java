package com.parkingbackuser;

import com.parkingbackuser.adapter.UserController;
import com.parkingbackuser.helper.UserControllerHelper;
import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserDto;
import com.parkingbackuser.ports.application.UserPort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;


import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith({MockitoExtension.class})
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserPort userPort;

    private UserControllerHelper userControllerHelper;

    @BeforeEach
    void initialize(){
        this.userControllerHelper = new UserControllerHelper();
    }

    @Test
    void createUserTest(){
        UserDto fake_userDto = userControllerHelper.getUserDto();
        userController.createUser(fake_userDto);
        verify(userPort, times(1)).createUser(any());
    }

    @Test
    void deleteUserTest() {
        UserDto fake_userDto = userControllerHelper.getUserDto();
        userController.deleteUser(fake_userDto.getDni(),fake_userDto.getMail());
        verify(userPort, times(1)).deleteUser(any(),any());
    }

    @Test
    void updateUserTest(){
        UserDto fake_userDto = userControllerHelper.getUserDto();

        userController.updateUser(fake_userDto.getDni(),fake_userDto.getMail(),fake_userDto);
        verify(userPort, times(1)).updateUser(anyString(),anyString(),any());
    }

    @Test
    void getUsersTest(){
        User user = userControllerHelper.getUser();

        when(userPort.getUsers(any())).thenReturn(List.of(user));

        UserDto userDto = userController.getUsers(any()).stream().findFirst().orElseThrow();

        assertEquals(user.getDni(),userDto.getDni());
        assertEquals(user.getMail(),userDto.getMail());
        assertEquals(user.getPassword(),userDto.getPassword());
        assertEquals(user.getPhone(),userDto.getPhone());
        assertEquals(user.getName(),userDto.getName());
        assertEquals(user.getLastName(),userDto.getLastName());

    }

}
