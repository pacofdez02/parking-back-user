package com.parkingbackuser.helper;

import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserDto;

public class UserControllerHelper {
    private String fake_dni;
    private String fake_mail;
    private String fake_password;
    private Integer fake_phone;
    private String fake_name;
    private String fake_lastName;

    void initializeVariables(){
        fake_dni = "20931845M";
        fake_mail = "pacofdez55@gmail.com";
        fake_password = "123456";
        fake_phone = 695811592;
        fake_name = "Paco";
        fake_lastName = "Fernandez";
    }

    public UserDto getUserDto(){
        initializeVariables();
        return UserDto.builder().dni(fake_dni).mail(fake_mail).password(fake_password).phone(fake_phone).name(fake_name).lastName(fake_lastName).build();
    }

    public User getUser(){
        initializeVariables();
        return User.builder().dni(fake_dni).mail(fake_mail).password(fake_password).phone(fake_phone).name(fake_name).lastName(fake_lastName).build();
    }
}
