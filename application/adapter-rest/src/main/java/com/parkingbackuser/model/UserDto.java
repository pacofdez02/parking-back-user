package com.parkingbackuser.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String dni;
    private String mail;
    private String password;
    private Integer phone;
    private String name;
    private String lastName;

}
