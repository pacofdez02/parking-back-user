package com.parkingbackuser.adapter.api;

import com.parkingbackuser.model.FilterUserDto;
import com.parkingbackuser.model.UserDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/v1/user")

public interface UserApi {
    @GetMapping("/{dni}")
    @CrossOrigin
    UserDto getUser(@PathVariable String dni, @RequestParam String mail);

    @PostMapping("/users")
    @CrossOrigin
    List<UserDto> getUsers(@RequestBody FilterUserDto userFilterDto);

    @PostMapping()
    @CrossOrigin
    void createUser(@RequestBody UserDto userDto);

    @RequestMapping(value = "/{dni}", method = RequestMethod.DELETE)
    @CrossOrigin
    void deleteUser(@PathVariable String dni, @RequestParam String mail);

    @PutMapping("/{dni}")
    @CrossOrigin
    void updateUser(@PathVariable String dni, @RequestParam String mail, @RequestBody UserDto userDto);

    @GetMapping("/login")
    @CrossOrigin
    UserDto loginUser(@RequestParam String mail, @RequestParam String password);
}
