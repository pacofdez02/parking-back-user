package com.parkingbackuser.adapter;

import com.parkingbackuser.adapter.api.UserApi;
import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.FilterUserDto;
import com.parkingbackuser.model.User;
import com.parkingbackuser.model.UserDto;
import com.parkingbackuser.mapper.UserMapper;
import com.parkingbackuser.ports.application.UserPort;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserPort userPort;
    private final UserMapper userMapper = UserMapper.INSTANCE;

    @Override
    public UserDto getUser(String dni, String mail) {
        return userMapper.toDto(userPort.getUser(dni,mail));
    }

    @Override
    public List<UserDto> getUsers(FilterUserDto filterUserDto){
        return userPort.getUsers(userMapper.toDomain(filterUserDto)).stream().map(userMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void createUser(UserDto userDto) {
        userPort.createUser(userMapper.toDomain(userDto));
    }

    @Override
    public void deleteUser(@PathVariable String dni, @RequestParam String mail) {
        userPort.deleteUser(dni,mail);
    }

    @Override
    public void updateUser(@PathVariable String dni, @RequestParam String mail, @RequestBody UserDto userDto){
        userPort.updateUser(dni,mail, userMapper.toDomain(userDto));
    }


    @Override
    public UserDto loginUser(String mail, String password){
        return userMapper.toDto(userPort.loginUser(mail,password));
    }


}
