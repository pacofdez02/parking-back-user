package com.parkingbackuser.mapper;

import com.parkingbackuser.model.FilterUser;
import com.parkingbackuser.model.FilterUserDto;
import com.parkingbackuser.model.UserDto;
import com.parkingbackuser.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User toDomain (UserDto userDto);
    UserDto toDto (User user);

    FilterUser toDomain(FilterUserDto filterUserDto);


}
