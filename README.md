# Parking-back-user

## Name
Parking User

## Description
This project manages a CRUD of Users from a parking

## Installation

mvn clean install -U

Docker:
docker run --name parking -e POSTGRES_PASSWORD=root -e POSTGRES_USER=sa -e POSTGRES_DB=parking-user -p 5432:5432 -d postgres


## License
Creative Commons (CC)
